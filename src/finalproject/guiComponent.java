/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finalproject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.text.AttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import org.json.JSONObject;

public class guiComponent extends JFrame {
  private final static JMenuBar menuBar = new JMenuBar();
  private final static JMenu menu = new JMenu("Fichier");
  private final static JMenu menu_2 = new JMenu("Sous ficher");
  private final static JMenu menu_3 = new JMenu("Edition");

  private final static JMenuItem item1 = new JMenuItem("Old search");
  private final static JMenuItem item2 = new JMenuItem("Fermer");
  private final static JMenuItem item3 = new JMenuItem("Lancer");
  private final static JMenuItem item4 = new JMenuItem("Arrêter");
  private static final JLabel label_icon = new JLabel("");
  private static final JLabel label_search = new JLabel("Search an Image");
  private static final JLabel label_progress = new JLabel("Recherche en cours...");
  private static JProgressBar barDo = new JProgressBar(0,100);
  private final static JPanel panel = new JPanel();
  private final static JTextField textfield = new JTextField();
  private final static JButton btn = new JButton("Go");
  private static String[] list_cat = {"animals","nature","fruits","food","vegetables","cars",
        "flowers","clothing","objects","people","fantasy","technic","furniture",
        "numbers","alphabet","sport","insect","tableware","logos","electronics",
        "transport","weapons","jewelry","miscellaneous"};
  
  static int counter = 0;
  static Timer timer;
  ExecutorService executorService = Executors.newSingleThreadExecutor();
  

  

    public guiComponent(){
        this.barDo.setBounds(380,300,200, 150);
        this.barDo.setStringPainted(true);
        
    this.textfield.setBounds(180,180,400,35);
    this.btn.setBounds(585,180,60,35);
    this.label_search.setBounds(60,180,120,35);
    this.label_progress.setBounds(60,180,120,35);
    this.label_icon.setBounds(300,70,180,90);
    ImageIcon image = new ImageIcon("C:\\Users\\edou5\\Pictures\\bug lekolpam\\icon.PNG");
    this.label_icon.setIcon(image);
    //On initialise nos menus      
    this.menu.add(item1);

    //On ajoute les éléments dans notre sous-menu
    
    //Ajout d'un séparateur
    this.menu_2.addSeparator();
    //On met nos radios dans un ButtonGroup
    
    //On présélectionne la première radio
    

    //Ajout du sous-menu dans notre menu
    this.menu.add(this.menu_2);
    //Ajout d'un séparateur
    this.menu.addSeparator();
    
    item1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                int result = fc.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        new NewClass().image.setIcon(new ImageIcon(ImageIO.read(file)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    
    item2.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
      }        
    });
    btn.addActionListener(new ActionListener(){
            private int dialogButton;
        public void actionPerformed(ActionEvent arg0){
            if(pingHost("www.google.com",80,3000)==false){
                JOptionPane.showConfirmDialog(null,
                        "No Internet Connection, The reseach online is not Available", 
                        "Error ",JOptionPane.ERROR_MESSAGE);
            }
            else{
                try {
                    
                    //link2();
                    String getText = textfield.getText();
                    String text = getText.replace(".", "-");
                    String text_final = text.replace(" ", "-");
                    File dir = new File(text_final);
                    if(!dir.exists()){
                        dir.mkdir();
                        link();
                        System.out.println("directory created");
                        dialogButton = JOptionPane.showConfirmDialog (null,
                                "Download task is complete\n"+"Do you want to see the results?","Info", dialogButton);
                        if(dialogButton == JOptionPane.YES_OPTION) {
                            JFileChooser jfc = new JFileChooser(dir);
                            int returnValue = jfc.showOpenDialog(null);
                            if (returnValue == JFileChooser.APPROVE_OPTION) {
                                    File selectedFile = jfc.getSelectedFile();
                                    new NewClass().image.setIcon(new ImageIcon(ImageIO.read(selectedFile)));
                                    System.out.println(selectedFile.getAbsolutePath());
                            }
                            //readfile(dir);
                        }
                        else {
                            remove(dialogButton);
                        }
                        
                    }
                    else{
                         JOptionPane.showConfirmDialog(null,
                                 "This research has already been carried out\n"+"do u want to verify ",
                                 "reseach already carried out",JOptionPane.INFORMATION_MESSAGE);
                    }
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                textfield.setText("");
            }
        }
    });
    
    
    panel.setBackground(Color.white);
    panel.setLayout(null);
    panel.add(label_icon, BorderLayout.CENTER);
    panel.add(label_icon, BorderLayout.CENTER);
    panel.add(label_search, BorderLayout.CENTER);
    panel.add(textfield, BorderLayout.CENTER);
    panel.add(btn, BorderLayout.CENTER);
    this.menu.add(item2);  
    this.menu_3.add(item3);
    this.menu_3.add(item4);
    
    

    //L'ordre d'ajout va déterminer l'ordre d'apparition dans le menu de gauche à droite
    //Le premier ajouté sera tout à gauche de la barre de menu et inversement pour le dernier
    this.menuBar.add(menu);
    this.menuBar.add(menu_3);
    this.setJMenuBar(menuBar);
    this.add(panel);
    this.setContentPane(panel);
    this.setVisible(true);
  }
     

    public void readfile(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                readfile(fileEntry);
            } else {
                System.out.println(fileEntry.getName());
            }
        }
    }

    public void link() throws IOException{
        
          String getText = textfield.getText();
          String uri = "http://pngall.com/";
          String text = getText.replace(".", "-");
          String text_final = text.replace(" ", "-");
          File dir = new File(text_final);        
          getImages(uri+text_final);
                           
    }
    
    public static void link2() throws IOException{
        String getText = textfield.getText();
        String uri = "http://pngimg.com/img/";
        getImages("http://pngimg.com/img/animals/cow");
        for(String cat: list_cat){
            //getImages(uri+cat+"/"+getText);
                System.out.println(uri+cat+"/"+getText);
            if (getText==cat){
                getImages("http://pngimg.com/img/"+cat);
            } else {
                
            }
        }
    }
    //get all images from  link url
    public static void getImages(String s) throws MalformedURLException, IOException{
        
        try
          {
              //String webUrl = "http://pngimg.com/img/animals/camel";
              
              URL url = new URL(s);
              URLConnection connection = url.openConnection();
              HttpURLConnection httpcon = (HttpURLConnection) url.openConnection(); 
              httpcon.addRequestProperty("User-Agent", "Chrome"); 
              //connection.setRequestProperty("User-Agent", "Chrome");
              InputStream is = httpcon.getInputStream();
              InputStreamReader isr = new InputStreamReader(is);
              BufferedReader br = new BufferedReader(isr);

              HTMLEditorKit htmlKit = new HTMLEditorKit();
              HTMLDocument htmlDoc = (HTMLDocument) htmlKit.createDefaultDocument();
              HTMLEditorKit.Parser parser = new ParserDelegator();
              HTMLEditorKit.ParserCallback callback = htmlDoc.getReader(0);
              parser.parse(br, callback, true);

              for (HTMLDocument.Iterator iterator = htmlDoc.getIterator(HTML.Tag.IMG); iterator.isValid(); iterator.next()) {
                  AttributeSet attributes = iterator.getAttributes();
                  String imgSrc = (String) attributes.getAttribute(HTML.Attribute.SRC);
                  System.out.println("URL:  "+imgSrc);
                  URL u = new URL(imgSrc);
                  DownloadImages(s,imgSrc);
              }
          } 
          catch(Exception e)
          {
              JOptionPane.showMessageDialog(null, e.getMessage(), "Failure", JOptionPane.ERROR_MESSAGE);
              e.printStackTrace();
          }

         }  
    
    
    //extract image form url and download it
    public static void DownloadImages(String url,String imgSrc){
        BufferedImage image = null;
        try {
            
            String imageFormat = "png";
            
            if (!(imgSrc.startsWith("http"))) {
                url = url + imgSrc;
            } else {
                url = imgSrc;
            }
            imgSrc = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);
            
            imageFormat = imgSrc.substring(imgSrc.lastIndexOf(".") + 1);
            String getText = textfield.getText();
            String text = getText.replace(".", "-");
            String text_final = text.replace(" ", "-");
            File dir = new File(text_final);
            dir.mkdir();
            URL imageUrl = new URL(url);
            HttpURLConnection httpcon = (HttpURLConnection) imageUrl.openConnection();
            httpcon.addRequestProperty("User-Agent", "Chrome/4.76");
            image = ImageIO.read(httpcon.getInputStream());
            
            ImageIO.write(image, imageFormat, new File(dir+"/"+imgSrc+""));
            System.out.println("Done");
            System.out.println("phase manquante (Download)");
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
        
    }
    
    //verify internet connection (Method)
    public static boolean pingHost(String host, int port, int timeout) {
    try (Socket socket = new Socket()) {
        socket.connect(new InetSocketAddress(host, port), timeout);
        
        return true;
    } catch (IOException e) {
        
        return false;
        
    }
}
    
    public static boolean exists(String URLName){
    try {
      Properties systemSettings = System.getProperties();
      systemSettings.put("proxySet", "true");
      systemSettings.put("http.proxyHost","proxy.mycompany.local") ;
      systemSettings.put("http.proxyPort", "80") ;

      URL u = new URL(URLName);
      HttpURLConnection con = (HttpURLConnection) u.openConnection();
      //
      // it's not the greatest idea to use a sun.misc.* class
      // Sun strongly advises not to use them since they can 
      // change or go away in a future release so beware.
      //
      sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
      String encodedUserPwd =
         encoder.encode("domain\\username:password".getBytes());
      con.setRequestProperty
         ("Proxy-Authorization", "Basic " + encodedUserPwd);
      con.setRequestMethod("HEAD");
      System.out.println
         (con.getResponseCode() + " : " + con.getResponseMessage());
      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
    
    class NewClass {
        private JFrame      j;
        private JMenu       jmenu;
        private JMenuBar    jbar;
        private JMenuItem   jmi, jexit;
        private JPanel      jpanel;
        private JButton     jpre, jnext;
        JLabel              image;
        ImageIcon           ic;
        Image               img;

        NewClass() {
            j = new JFrame("Image Viewer");
            j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            j.setLocationRelativeTo(j);

            // j.setExtendedState(Frame.MAXIMIZED_BOTH);
            // j.setLocationRelativeTo(null);
            j.setLocationByPlatform(true);
            j.setLayout(new GridBagLayout());

            
            jpanel = new JPanel();
            jpanel.setLayout(null);
            jpanel.setBackground(Color.white);
            image = new JLabel(" ",JLabel.CENTER);
            image.setBounds(50,50,850,500);
            jpre = new JButton("Previous");
            jpre.setBounds(350,600,90,40);
            jnext = new JButton("Next");
            jnext.setBounds(450,600,90,40);
            jpanel.add(image, BorderLayout.CENTER);
            jpanel.add(jpre, BorderLayout.CENTER);
            jpanel.add(jnext, BorderLayout.CENTER);
            j.add(jpanel);
            j.setContentPane(jpanel);

            jbar = new JMenuBar();
            jmenu = new JMenu("File");
            jmi = new JMenuItem("Open");
            jmi.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    JFileChooser fc = new JFileChooser();
                    int result = fc.showOpenDialog(null);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        try {
                            image.setIcon(new ImageIcon(ImageIO.read(file)));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            jexit = new JMenuItem("Exit");
            jexit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    System.exit(0);
                }
            });
            jmenu.add(jmi);
            jmenu.add(jexit);
            jbar.add(jmenu);
            j.setJMenuBar(jbar);
            j.setSize(900, 700);
            j.setResizable(true);
            j.setVisible(true);
        }
    }
}