/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finalproject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 *
 * @author edou5
 */
public class FinalProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        pingHost("www.google.com",80,3000);
        new MainWindow();
        
    }
    
    public static boolean pingHost(String host, int port, int timeout) {
    try (Socket socket = new Socket()) {
        socket.connect(new InetSocketAddress(host, port), timeout);
        System.out.println("Internet Connection is ok");
        return true;
    } catch (IOException e) {
        System.out.println("Internet Connection issues");
        return false; // Either timeout or unreachable or failed DNS lookup.
        
    }
}
    
}
